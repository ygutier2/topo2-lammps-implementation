# LAMMPS implementation of TopoII

We model a torsionally relaxed (nicked) DNA plasmid 7.2 kbp-long as a bead-spring polymer made of 1000 beads with bead-size σ=2.5 nm=7.3 bp and connected by finitely extensible springs. The persistence length of the polymer is set to lp=5σ. Beads interact via a purely
steric Lennard-Jones repulsion while the TopoII-bound segment is modeled as a 50 beads (L=360 bp)-long segment displaying soft interactions with the other beads thereby allowing the TopoII-bound segment to undergo strand-crossing with a small energy penalty (2 k<sub>B</sub>T). For details in the model and the system used we refer the reader to our paper: **Dynamic and facilitated binding of topoisomerase accelerates topological relaxation** 
*Davide Michieletto,  Yair A G Fosado,  Elias Melas,  Marco Baiesi,  Luca Tubiana, Enzo Orlandini*, **Nucleic Acids Research**, Volume 50, Issue 8, 6 May 2022, Pages 4659–4668, https://doi.org/10.1093/nar/gkac260

All the simulation scripts in this repository are set to start from a N=1000 beads thermally equilibrated polymer tied in a 5<sub>1</sub> torus knot. This and other initial configurations are provided inside the folder **0.InitialConfigurations**. The files are written in the correct format to be read by LAMMPS.

The position of the TopoII-bound segment can be either: 

* **1.Static**: Meaning that at the beginning of the simulation (t=0), the position of the topoII is chosen randomly and then it stays in the same position during the course of the simulation.
* **2.Random Jump**: The position of the topoII is updated with a certain frequency and it is placed randomly along the contour length of the polymer.
* **3. Jump to maximum density**: The position of the topoII is updated with a certain frequency and it is placed in the site of maximum density of beads along the contour length of the polymer.

A brief explanation of the implementation in each of these cases and the scripts to run these simulations are provided inside the corresponding folders in this repository. All the LAMMPS scripts have been tested using the current stable-version of LAMMPS (29-October-2020). To built the LAMMPS executables please make sure that the MOLECULE package is installed. This is a standard package that come with LAMMPS, and can be enabled before compiling, e.g. with the command: make yes-molecule.

In addition, we provide in **4.FixTopoII** the implementation of the same system using a new fix (**fix topoII**) written for LAMMPS. We have checked that when running in serial, this fix provides a 30%-35% speed up in comparison with the simulation using the external code. Also, this new fix reduces considerably the hard drive space needed to store the output from simulations. Just to give an example, running 32 replicas of a system that consists of 1000 beads, for 300 000 000 time-steps and dumping data every 100 000 time-steps; the external program requires 288 GB of space, while the fixtopo2 only requires 14.5 GB. This is because when running with the external program, every time LAMMPS reads a file (generated by the external programm), it dumps all the atom information to the *log.lammps* file. Creating in this way huge *log.lammps* files. 

Please note that we refer to the executable with the standard packages as *lmp_mpi_29Oct2020*. This will be necessary to run the scripts with the external code (in folders 1 to 3). The simulations inside folder 4 require the custom fix. Further instructions on how to compile the corresponding LAMMPS executables (*lmp_mpi_29Oct2020_fixtopo2* and *lmp_serial_29Oct2020_fixtopo2*) can be found in the README files inside folder 4. 
