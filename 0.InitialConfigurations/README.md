# Initial configurations

In this folder you will find different configurations that have been previously equilibrated and that are written in a format compatible with LAMMPS. The files correspond to the following systems:

* **Eq.5_1.N1000.data**: Represents a thermally equilibrated polymer made of 1000 beads and tied in a 5<sub>1</sub> torus knot.
* **Equilibrated.5_1.N2000.data**: Represents a thermally equilibrated polymer made of 2000 beads and tied in a 5<sub>1</sub> torus knot. 
* **Equilibrated.3_1.N1000.data**: Represents a thermally equilibrated polymer made of 2000 beads and tied in a 3<sub>1</sub> torus knot.
* **Equilibrated.0_1.N1000.data**: Represents a thermally equilibrated polymer made of 2000 beads and that is unknotted.
