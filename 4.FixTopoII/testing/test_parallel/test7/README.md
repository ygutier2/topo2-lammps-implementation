# Test 7 in parallel: The beads in the topoII section change type (from 1 to 4). Type 4 interacts differently than type 1.

In this test we run two simulations:

* 1.- Without introducing a topoII into the system (**run.lam.test.without**): In this case we use simply *fix nve + fix langevin* to run a Brownian Dynamics simulation. Only beads of type 1 are present into the system.
* 2.- Introducing a topoII into the system (**run.lam.test.with**): In this case, besides the two previous fixes we use  *fix_topo2* with the following parameters:
  * a) *n = 1*: the number of topoII regions.
  * b) *l = 50 Ï*: is the length of consecutive beads defining the topoII section. 
  * c) *type = 4*: the beads in the section where the **topoII is jumping to** will be assigned this atom type.
  * d) *seed:* to generate randomly the position of the topoII section
  * e1) For the **static** keyword of this fix, in addition to (a)-(d) we need to set at which timestep the topoII will be introduced, this is *tin = 50*.
  * e2) For the **randjump** and **jump2maxdens** keywords of this fix, in addition to (a)-(d) we need to set :
    * The frequency at which the fix will be called (*freq=10*) 
    * The probability that when the fix is called the topoII section changes position (*prob=1.0*). 
    * The bead type that it will be temporally assigned to the section that the **topoII is jumping from** (*type=35).

Both simulations were run with the exact same seed for the Langevin fix to be able to compare the trajectory generated in the two cases. Also, both simulations start from the same initial configuration.

**Note 1:** Since the *fix_topo2* changes the atom type of the particles in the section where it is jumping to (to type 4) and the section is jumping from (to type 5); then because type 2 and type 3 particles interact sterically in a different way (soft repulsion) as particles type 1 (LJ repulsion), we expect that positions, velocities and forces are affected by this change. If this is the case when running **bash.run.sh** at the end the programm will print success.

**Note 2:** By default the lammps script with the topo2 (*run.lam.test.with*) sets the simulation with the keyword static. If you want to test a different keyword uncomment the lines related to fix_topo2 at the end of the file accordingly.
