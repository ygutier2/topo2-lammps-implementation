#!/bin/bash

#Directories
cwd=$(pwd)

#Executable
LAMMPS=~/lmp_mpi_29Oct2020_fixtopo2

#number of processors
np=4

#Set variables
topofreq=100  #frequency with which topo2 is intorduced
initial=0     #initial time of the simulation
last=10000    #End of simulation
dumpfreq=100  #Dump frequency
topotype=4    #atom type for the atoms in the topoII section
ltopo=50      #length of the topoII
check=0       #flag


#Run system without fix topo2:
  echo "Running system without TopoII"
  cd ${cwd}
  if [ -d "DUMPS_without_topo2" ]; then rm -Rf DUMPS_without_topo2; fi
  
  mpirun -np ${np} ${LAMMPS} -in run.lam.test.without
  mv energy_total.txt DUMPS/
  mv radius_of_gyration.txt DUMPS/
  mv log.lammps DUMPS/log.lammps
  mv DUMPS DUMPS_without_topo2


#Run system with fix topo2:
  echo "Running system with TopoII"
  cd ${cwd} 
  if [ -d "DUMPS_with_topo2" ]; then rm -Rf DUMPS_with_topo2; fi
  
  mpirun -np ${np} ${LAMMPS} -in run.lam.test.with
  mv energy_total.txt DUMPS/
  mv radius_of_gyration.txt DUMPS/
  mv log.cite DUMPS/
  mv log.lammps DUMPS/log.lammps1
  mv DUMPS DUMPS_with_topo2


#Compare results of the two simulations
#create an empty file:
if [ -f "results.txt" ] ; then
    rm "results.txt"
fi
touch results.txt

for t in $( seq 0 ${dumpfreq} ${last} )
do
  #cd ${cwd} 
  
  echo "timestep $t" >> results.txt


  #Files containing: id type mol mass x y z ix iy iz vx vy vz fx fy fz
  sim1="DUMPS_without_topo2/Run.R.${t}"
  sim2="DUMPS_with_topo2/Run.R.${t}"
  
  #Remove the first 10 lines of each file
   sed -i '1,9d' ${sim1}
   sed -i '1,9d' ${sim2}

  #Compare all the columns except ($2: type).
  #FNR==NR is performed when reading the first file.
  #{a[]; next}:       stores in a[] the columns of the first file and goes to the next line.
  #($1,$3,...) in a:  is evaluated when looping through the second file. It checks if the current line is within the a[] array
  #If all the columns (except column 2, related to the type) match between the two files, then print the first and second column of the second file and append it to results.txt
  awk 'NR==FNR{a[$1,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16];next} ($1,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) in a { print $1,$2;}' ${sim1} ${sim2} >> results.txt
done

#If all the columns match between the two files, then 11 lines (10 particles + 1 timestep) are printed per timestep into the file "results.txt".
#number of particles
np=1000
#number of lines per timestep
nl=$(( np + 1 ))
#Total number of lines if all the columns matched for all timesteps: (11*101=1111)
ntotal=$(( (np+1)*(((last-initial)/dumpfreq)+1) ))

#Now count the number of lines in the file "results.txt"
nlf=$(< "results.txt" wc -l)


echo ${ntotal}
echo ${nlf}

if [ $ntotal -eq $nlf ]; then
  echo "#####################"
  echo "   TEST 6: SUCCESS   "
  echo "#####################"
fi

if [ $ntotal -ne $nlf ]; then
  echo "##################"
  echo "   TEST 6: FAIL   "
  echo "##################"
fi
