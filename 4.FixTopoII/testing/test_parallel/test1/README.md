# Test 1 in parallel: check for static case

Here we check that the random id computed for the **static** case corresponds to the particle(s) that change atom type. 

In this test we run one simulations:

* 1.- Introducing a topoII into the system (**run.lam.test.with**): Besides *fix_langevin + fix_nve* we use  *fix_topo2* with the following parameters:
  * a) *n = 1*: the number of topoII regions.
  * b) *l = 50 Ï*: is the length of consecutive beads defining the topoII section. 
  * c) *type = 4*: the beads in the section where the **topoII is placed** will be assigned this atom type.
  * d) *seed:* to generate randomly the position of the topoII section
  * e) For the **static** keyword of this fix, in addition to (a)-(d) we need to set at which timestep the topoII will be introduced, this is *t' = 500*.

From the time (t'=500) at which the topoII is placed into the system and onwards the 50 randomly selected beads will be type 4. This bead type interacts sterically with a soft potential with energy 2k_B and cutoff radius r_c=1.1224, to allow strand crossing. The bash file, **bash.run.sh** contains the instructions to run a short simulation 10000 time-steps long, and dumping files every 100 time-steps. After the end of simulation, also checks that this type change occurs as mentioned in here. In this case it prints the legend "Test 1: success".
