#!/bin/bash

#Directories
cwd=$(pwd)

#Executable
LAMMPS=~/lmp_mpi_29Oct2020_fixtopo2

#number of processors
np=4

############################
#Run system with fix topo2:#
############################
  if [ -f "static_fromLAMMPS.dat" ]; then rm "static_fromLAMMPS.dat"; fi
  echo "Running system with TopoII"
  cd ${cwd} 
  if [ -d "DUMPS_with_topo2" ]; then rm -Rf DUMPS_with_topo2; fi
  
  mpirun -np ${np} ${LAMMPS} -in run.lam.test.with
  mv energy_total.txt       DUMPS/
  mv radius_of_gyration.txt DUMPS/
  mv log.cite               DUMPS/
  mv log.lammps             DUMPS/log.lammps.static

  mv DUMPS DUMPS_with_topo2
  
  
#######################
#Check for static case#
#######################
tprime=500    #timestep at which the topo2 was introduced
initial=0     #initial time of the simulation
last=10000    #End of simulation
dumpfreq=100  #Dump frequency
topotype=4    #atom type for the atoms in the topoII section
ltopo=50    #length of the topoII

#create two empty file:
if [ -f "results1.txt" ]; then rm "results1.txt"; fi
touch results1.txt

if [ -f "results2.txt" ]; then rm "results2.txt"; fi
touch results2.txt



for t in $( seq 0 ${dumpfreq} ${last} )
do 
  #File with the id randomly chosen produced by LAMMPS
  fl="static_fromLAMMPS.dat"

  #Files containing: id type mol mass x y z ix iy iz vx vy vz fx fy fz
  sim="DUMPS_with_topo2/Run.R.${t}"
  
  #Remove the first 10 lines of the previous file
   sed -i '1,9d' ${sim}
    
  #Compare the second column ($2, with the atom-id randomly chosen) and ($3 with atom type) from file1 ($fl).
  #FNR==NR is performed when reading the first file.
  #{a[]; next}: stores in a[] the columns $2,$3 of the first file and goes to the next line.
  #($1,$2) in a:   The atom-id ($1) and type($2) from file2. Then it check it the atom id and type from file 1, matches the atomid in file 2. 
  #That is, it checks if the current line is within the a[] array.
  #If columns $1,$2 of first file matches columns $2,$3 of second file, then prints the first and second column of the second file and append it to results1.txt
  awk -v time="${t}" 'NR==FNR{a[$2,$3];next} ($1,$2) in a { print time,$1,$2;}' ${fl} ${sim} >> results1.txt
  
  #Compare the third column ($3) with the atom-type of topoII from file1 ($fl) with the atomtype(column $2) of the second file
  awk -v time="${t}" 'NR==FNR{a[$3];next} ($2) in a { print time,$1,$2;}' ${fl} ${sim} >> results2.txt
done


#Predicted number of lines in file "results1", if everything works smoothly
nl1=$(( ((last-tprime)/dumpfreq)+1 ))

#Now count the number of lines in the file "results1.txt"
nlf1=$(< "results1.txt" wc -l)

echo ${nl1}
echo ${nlf1}
  
#Predicted number of lines in file "results2", if everything works smoothly
nl2=$(( ltopo*nl1 ))

#Now count the number of lines in the file "results2.txt"
nlf2=$(< "results2.txt" wc -l)

echo ${nl2}
echo ${nlf2}


if [ $nl1 -eq $nlf1 ]; then
  if [ $nl2 -eq $nlf2 ]; then
     echo "########################"
     echo "   TEST 1: SUCCESS :)   "
     echo "########################"
  fi
fi

if [ $nl1 -ne $nlf1 ]; then
  echo "########################"
  echo "   TEST 1: FAIL :(   "
  echo "########################"
fi

if [ $nl2 -ne $nlf2 ]; then
  echo "########################"
  echo "   TEST 1: FAIL :(   "
  echo "########################"
fi
