#!/bin/bash

#Directories
cwd=$(pwd)

#Executable
LAMMPS=~/lmp_mpi_29Oct2020_fixtopo2


#number of processors
np=4


############################
#Run system with fix topo2:#
############################
  if [ -f "randjump_fromLAMMPS.dat" ]; then rm "randjump_fromLAMMPS.dat"; fi
  echo "Running system with TopoII"
  cd ${cwd} 
  if [ -d "DUMPS_with_topo2" ]; then rm -Rf DUMPS_with_topo2; fi
  
  mpirun -np ${np} ${LAMMPS} -in run.lam.test.with
  mv energy_total.txt       DUMPS/
  mv radius_of_gyration.txt DUMPS/
  mv log.cite               DUMPS/
  mv log.lammps             DUMPS/log.lammps.randjump

  mv DUMPS DUMPS_with_topo2
  
  
#######################
#Check for randjump case#
#######################
topofreq=500  #frequency with which topo2 is intorduced
initial=0     #initial time of the simulation
last=10000    #End of simulation
dumpfreq=100  #Dump frequency
topotype=4    #atom type for the atoms in the topoII section
ltopo=50      #length of the topoII

#create two empty file:
if [ -f "results1.txt" ]; then rm "results1.txt"; fi
touch results1.txt

if [ -f "results2.txt" ]; then rm "results2.txt"; fi
touch results2.txt


#File with the id randomly chosen produced by LAMMPS
fl="randjump_fromLAMMPS.dat"

#store the value of column $1 (timestep at which the programm was called)
timefl=( $(awk '{print $1}' ./randjump_fromLAMMPS.dat) )

#Check the files at those timestep
line=0
for t in "${timefl[@]}"
do
  line=$(( line+1 ))

  #Files containing: id type mol mass x y z ix iy iz vx vy vz fx fy fz
  sim="DUMPS_with_topo2/Run.R.${t}"
  #Remove the first 10 lines of the previous file
  sed -i '1,9d' ${sim}
  
  #Compare the second column ($2, with the atom-id randomly chosen) and ($3 with atom type) from file1 ($fl).
  #FNR==line is performed when reading the first file that specific line.
  #{a[]; next}: stores in a[] the columns $2,$3 of the first file and goes to the next line.
  #($1,$2) in a:   The atom-id ($1) and type($2) from file2. Then it checks if the atom id and type from file 1, matches the atomid in file 2. 
  #That is, it checks if the current line is within the a[] array.
  #If columns $1,$2 of first file matches columns $2,$3 of second file, then prints the first and second column of the second file and append it to results1.txt
  awk -v l=${line} -v time="${t}" 'NR==l{a[$2,$3];next} ($1,$2) in a { print time,$1,$2;}' ${fl} ${sim} >> results1.txt
  
  #Compare the third column ($3) with the atom-type of topoII from file1 ($fl) with the atomtype(column $2) of the second file
  awk -v l=${line} -v time="${t}" 'NR==l{a[$3];next} ($2) in a { print time,$1,$2;}' ${fl} ${sim} >> results2.txt
done


#Predicted number of lines in file "results1", if everything works smoothly
nl1=$(( ((last-tprime)/topofreq) ))

#Now count the number of lines in the file "results1.txt"
nlf1=$(< "results1.txt" wc -l)

echo ${nl1}
echo ${nlf1}
  
#Predicted number of lines in file "results2", if everything works smoothly
nl2=$(( ltopo*nl1 ))

#Now count the number of lines in the file "results2.txt"
nlf2=$(< "results2.txt" wc -l)

echo ${nl2}
echo ${nlf2}


if [ $nl1 -eq $nlf1 ]; then
  if [ $nl2 -eq $nlf2 ]; then
     echo "########################"
     echo "   TEST 2: SUCCESS :)   "
     echo "########################"
  fi
fi

if [ $nl1 -ne $nlf1 ]; then
  echo "########################"
  echo "   TEST 2: FAIL :(   "
  echo "########################"
fi

if [ $nl2 -ne $nlf2 ]; then
  echo "########################"
  echo "   TEST 2: FAIL :(   "
  echo "########################"
fi

