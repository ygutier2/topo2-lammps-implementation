#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include <unistd.h>
#include<functional>
#include<numeric>
#include<cstdlib>
#include <algorithm>
using namespace std;

int Ntot;
double Lmaxx,Lminx,Lmaxy,Lminy,Lmaxz,Lminz;
double Lx,Ly,Lz;
string dummy;
int id,type,mol,mass;
double x,y,z;
int ix,iy,iz;
double vx,vy,vz;
double fx,fy,fz;
//
const int Nbeads=1000;
double Polymer[Nbeads][3]; //x,y,z,type,id
double PolymerWrap[Nbeads][3]; //x,y,z
int PolymerImg[Nbeads][3]; //ix,iy,iz

double distance(double v1[6], double v2[6]);

int main(int argc, char* argv[]){
int time=atoi(argv[1]);
//ARGV[1]=timestep
//ARGV[2]=infile-no time
//ARGV[3]=topotype

int topotype=atoi(argv[3]);

//WRITE file for max density
stringstream writeFile;
writeFile <<"maxcurv_fromCPP.dat";
ofstream write;
write.open(writeFile.str().c_str(), std::ios_base::app);

//READ FILE
ifstream indata;
stringstream readFile;
readFile.str("");
readFile.clear();
readFile << argv[2] << argv[1];
indata.open(readFile.str().c_str());
//cout << readFile.str().c_str()<<endl;
if(!indata.good()){cout << "AHAHAHAHHA"<<endl; cin.get();cin.get();cin.get();}

//read 10 lines
for(int i=0;i<10;i++){
    if(i==3) {
        indata >> Ntot;
        //cout << "Ntot: "<< Ntot << endl;
    }
    if(i==5) {
        indata >> Lminx >> Lmaxx;
        Lx = Lmaxx-Lminx;
        //cout << "Lminx=" << Lminx << ", Lmaxx=" << Lmaxx << ", Lx=" << Lx << endl;
    }
    if(i==6) {
        indata >> Lminy >> Lmaxy;
        Ly = Lmaxy-Lminy;
        //cout << "Lminy=" << Lminy << ", Lmaxy=" << Lmaxy << ", Ly=" << Ly << endl;
    }
    if(i==7) {
        indata >> Lminz >> Lmaxz;
        Lz = Lmaxz-Lminz;
        //cout << "Lminz=" << Lminz << ", Lmaxz=" << Lmaxz << ", Lz=" << Lz << endl;
    }
    else{getline(indata,dummy);}
}


//READ ATOMS
for(int n=0; n<Ntot; n++){
	indata >> id >> type >> mol >> mass >> x >> y >> z >> ix >> iy >> iz >> vx >> vy >> vz >> fx >> fy >> fz;
	PolymerWrap[id-1][0]=x;
	PolymerWrap[id-1][1]=y;
	PolymerWrap[id-1][2]=z;
	
	PolymerImg[id-1][0]=ix;
	PolymerImg[id-1][1]=iy;
	PolymerImg[id-1][2]=iz;
	
	Polymer[id-1][0]=x+Lx*ix;
	Polymer[id-1][1]=y+Ly*iy;
	Polymer[id-1][2]=z+Lz*iz;
}

//for(int n=0; n<Ntot; n++){
   // cout << n+1 << " " << PolymerWrap[n][0] << " " << PolymerWrap[n][1] << " " << PolymerWrap[n][2] << endl;
//}

////////////////////////////////////////////////////////////////////////
//////////// COMPUTE LOCAL CURVATURE w sliding window //////////////////////////////////
//////////////////////////////////////////////////////////////////////
//this is a reasonable size to accommodate a knot 
// anything shorter would be not informative 
int w=50;

int loc;
double maxcurv=0;
double localcurv[Nbeads];
for(int i=0; i<Nbeads;i++)localcurv[i]=0;

for (int i=0; i<Nbeads;i++) {
localcurv[i]=0;

	//loop  over window centered at i
	for (int j=0; j<=w;j++) {
	double tang1[3];
	double tang2[3];

	int b1=int(i+j-w/2.)-1;
	int b2=int(i+j-w/2.);
	int b3=int(i+j-w/2.)+1;
	if(b1<0)b1=Nbeads+b1; // [0:Nbeads-1]
	if(b2<0)b2=Nbeads+b2; // [0:Nbeads-1]
	if(b3<0)b3=Nbeads+b3; // [0:Nbeads-1]
	if(b1>=Nbeads)b1=b1%Nbeads; // [0:Nbeads-1]
	if(b2>=Nbeads)b2=b2%Nbeads; // [0:Nbeads-1]
	if(b3>=Nbeads)b3=b3%Nbeads; // [0:Nbeads-1]

	for(int d=0; d<3; d++){
	tang1[d] = Polymer[b2][d]-Polymer[b1][d];
	tang2[d] = Polymer[b3][d]-Polymer[b2][d];
	
	//tang1[d] = PolymerWrap[b2][d]-PolymerWrap[b1][d];
	//tang2[d] = PolymerWrap[b3][d]-PolymerWrap[b2][d];
	}

	double norm1=sqrt(tang1[0]*tang1[0]+tang1[1]*tang1[1]+tang1[2]*tang1[2]);
	double norm2=sqrt(tang2[0]*tang2[0]+tang2[1]*tang2[1]+tang2[2]*tang2[2]);

	double costheta=(tang1[0]*tang2[0]+tang1[1]*tang2[1]+tang1[2]*tang2[2])/(norm1*norm2);

	//integrate over window
	//divide by w to get the "average"
	localcurv[i]+=(1.0-costheta)*1.0/w; 
	}

if(localcurv[i]>maxcurv){
maxcurv=localcurv[i];
loc=i+1;
}


}//closing i

write << time << " " << loc << " " << topotype << endl;


return 0;

}

double distance(double v1[6], double v2[6]){
double d=0;

d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);

return sqrt(d);
}
