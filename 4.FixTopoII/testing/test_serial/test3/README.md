# Test 3 in serial: check for jump2maxdens case

Here we check that the atom id computed for the **jump to maximum density** case it is the same as the one computed by the external programm

In this test we run one simulation:

* 1.- Introducing a topoII into the system (**run.lam.test.with**): Besides *fix_langevin + fix_nve* we use  *fix_topo2* with the following parameters:
  * a) *n = 1*: the number of topoII regions.
  * b) *l = 50 Ï*: is the length of consecutive beads defining the topoII section. 
  * c) *type = 4*: the beads in the section where the **topoII is placed** will be assigned this atom type.
  * d) *seed:* not needed for this case.
  * e) For the **jump2maxdens** keyword of this fix, in addition to (a)-(d) we need to set :
    * The frequency at which the fix will be called (*freq=500*) 
    * The probability that when the fix is called the topoII section changes position (*prob=1.0*). 
    * The bead type that it will be temporally assigned to the section that the **topoII is jumping from** (*type=5*).

From the time (t'=500) at which the topoII is placed into the system and onwards the 50 randomly selected beads will be type 4. This bead type interacts sterically with a soft potential with energy 2k_B and cutoff radius r_c=1.1224, to allow strand crossing. The bash file, **bash.run.sh** contains the instructions to run a short simulation 10000 time-steps long, and dumping files every 100 time-steps. After the end of simulation, also checks that this type change occurs as mentioned in here. In this case it prints the legend "Test 3: success".



**IMPORTANT:** we use the wrapped coordinates to compute the local density
