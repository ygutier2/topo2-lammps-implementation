#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include <unistd.h>
#include<functional>
#include<numeric>
#include<cstdlib>
#include <algorithm>
using namespace std;

int Ntot;
double Lmaxx,Lminx,Lmaxy,Lminy,Lmaxz,Lminz;
double Lx,Ly,Lz;
string dummy;
int id,type,mol,mass;
double x,y,z;
int ix,iy,iz;
double vx,vy,vz;
double fx,fy,fz;
//
const int Nbeads=1000;
double Polymer[Nbeads][3]; //x,y,z,type,id
double PolymerWrap[Nbeads][3]; //x,y,z
int PolymerImg[Nbeads][3]; //ix,iy,iz

double distance(double v1[6], double v2[6]);

int main(int argc, char* argv[]){
int time=atoi(argv[1]);
//ARGV[1]=timestep
//ARGV[2]=infile-no time
//ARGV[3]=topotype

int topotype=atoi(argv[3]);

//WRITE file for max density
stringstream writeFile;
writeFile <<"maxdens_fromCPP.dat";
ofstream write;
write.open(writeFile.str().c_str(), std::ios_base::app);

//READ FILE
ifstream indata;
stringstream readFile;
readFile.str("");
readFile.clear();
readFile << argv[2] << argv[1];
indata.open(readFile.str().c_str());
//cout << readFile.str().c_str()<<endl;
if(!indata.good()){cout << "AHAHAHAHHA"<<endl; cin.get();cin.get();cin.get();}

//read 10 lines
for(int i=0;i<10;i++){
    if(i==3) {
        indata >> Ntot;
        //cout << "Ntot: "<< Ntot << endl;
    }
    if(i==5) {
        indata >> Lminx >> Lmaxx;
        Lx = Lmaxx-Lminx;
        //cout << "Lminx=" << Lminx << ", Lmaxx=" << Lmaxx << ", Lx=" << Lx << endl;
    }
    if(i==6) {
        indata >> Lminy >> Lmaxy;
        Ly = Lmaxy-Lminy;
        //cout << "Lminy=" << Lminy << ", Lmaxy=" << Lmaxy << ", Ly=" << Ly << endl;
    }
    if(i==7) {
        indata >> Lminz >> Lmaxz;
        Lz = Lmaxz-Lminz;
        //cout << "Lminz=" << Lminz << ", Lmaxz=" << Lmaxz << ", Lz=" << Lz << endl;
    }
    else{getline(indata,dummy);}
}


//READ ATOMS
for(int n=0; n<Ntot; n++){
	indata >> id >> type >> mol >> mass >> x >> y >> z >> ix >> iy >> iz >> vx >> vy >> vz >> fx >> fy >> fz;
	PolymerWrap[id-1][0]=x;
	PolymerWrap[id-1][1]=y;
	PolymerWrap[id-1][2]=z;
	
	PolymerImg[id-1][0]=ix;
	PolymerImg[id-1][1]=iy;
	PolymerImg[id-1][2]=iz;
	
	Polymer[id-1][0]=x+Lx*ix;
	Polymer[id-1][1]=y+Ly*iy;
	Polymer[id-1][2]=z+Lz*iz;
}

//for(int n=0; n<Ntot; n++){
    //cout << n+1 << " " << PolymerWrap[n][0] << " " << PolymerWrap[n][1] << " " << PolymerWrap[n][2] << endl;
//}


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//////////// COMPUTE LOCAL DENSITY  //////////////////////////////////
//////////////////////////////////////////////////////////////////////
// choose a value of w within which it is reasonable to accommodate a knot 
// smaller values will not be informative
int w=50; 
int loc;
float maxdens=0;
double localdens[Nbeads];

for (int i=0; i<Nbeads;i++) {
    localdens[i]=0;

    //radius of sphere we're considering
    //use length of window to the power of 0.588 as per SAW
    double R=pow(w,0.588);

    //look at all beads and find those within radius
    for (int j=0;j<Nbeads;j++){
	       //if(distance(PolymerWrap[j],PolymerWrap[i])<R) localdens[i]++;
	       if(distance(Polymer[j],Polymer[i])<R) localdens[i]++;
    }
    //write <<time <<" " << i << " " << localdens[i]<<endl;

    if(localdens[i]>maxdens){
        maxdens=fabs(localdens[i]);
        loc=i+1;
    }
}

//The id of the bead at the centre of max density
write << time << " " << loc << " " << topotype << endl;

return 0;

}

double distance(double v1[3], double v2[3]){
double d=0;

d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);

return sqrt(d);
}
