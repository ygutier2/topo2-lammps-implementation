# Test 8 in serial: Check that restarting a simulation works

In this test we run 4 simulations:

* 1.- Without introducing a topoII into the system (**run.lam.test.without**): In this case we use simply *fix nve + fix langevin* to run Brownian dynamic simulations. We run the simulation for 10 000 time-steps and dumping every 100 time-steps.
* 2.- We continue the previous simulation using th script (**run.lam.test.without.restart**): starting from the last restart file created we run for another 10 000 time-steps. Therefore the total time from both simulations is 20 000 time-steps.
* 3.- Introducing a topoII into the system (**run.lam.test.with**): In this case, besides the previous two fixes we use  *fix_topo2* with the following parameters:
  * a) *n = 1*: the number of topoII regions.
  * b) *l = 50 Ï*: is the length of consecutive beads defining the topoII section. 
  * c) *type = 1*: the beads in the section where the **topoII is jumping to** will be assigned this atom type.
  * d) *seed:* to generate randomly the position of the topoII section
  * e1) For the **static** keyword of this fix, in addition to (a)-(d) we need to set at which timestep the topoII will be introduced, this is *tin = 50*.
  * e2) For the **randjump** and **jump2maxdens** keywords of this fix, in addition to (a)-(d) we need to set :
    * The frequency at which the fix will be called (*freq=100*) 
    * The probability that when the fix is called the topoII section changes position (*prob=1.0*). 
    * The bead type that it will be temporally assigned to the section that the **topoII is jumping from** (*type=1*).
* 3.- We continue the previous simulation from the last restart file created (**run.lam.test.with.restart**)

All simulations were run with the exact same seed for the Langevin fix to be able to compare the trajectory generated in the two cases. Also, both simulations start from the same initial configuration.

**Note 1:** Since the *fix_topo2* in reality is not changing the atom type of the particles in the topoII section, both simulations should give the exact same results. If this is the case when running **bash.run.sh** at the end the programm will print success.

**Note 2:** By default the two lammps script with the topo2 (*run.lam.test.with and run.lam.test.with.restart*) set the simulation with the keyword static. If you want to test a different keyword uncomment the lines related to fix_topo2 at the end of the file accordingly.
