# Test 5 in serial: The topoII type is equal to the rest of the beads

In this test we run two simulations:

* 1.- Without introducing a topoII into the system (**run.lam.test.without**): In this case we use simply *fix nve + fix langevin* to run a Brownian Dynamics simulation.
* 2.- Introducing a topoII into the system (**run.lam.test.with**): In this case, besides the two previous fixes we use  *fix_topo2* with the following parameters:
  * a) *n = 1*: the number of topoII regions.
  * b) *l = 50 Ï*: is the length of consecutive beads defining the topoII section. 
  * c) *type = 1*: the beads in the section where the **topoII is jumping to** will be assigned this atom type.
  * d) *seed:* to generate randomly the position of the topoII section
  * e1) For the **static** keyword of this fix, in addition to (a)-(d) we need to set at which timestep the topoII will be introduced, this is *tin = 50*.
  * e2) For the **randjump** and **jump2maxdens** keywords of this fix, in addition to (a)-(d) we need to set :
    * The frequency at which the fix will be called (*freq=10*) 
    * The probability that when the fix is called the topoII section changes position (*prob=1.0*). 
    * The bead type that it will be temporally assigned to the section that the **topoII is jumping from** (*type=1*).

Both simulations were run with the exact same seed for the Langevin fix to be able to compare the trajectory generated in the two cases. Also, both simulations start from the same initial configuration.

**Note 1:** Since the *fix_topo2* in reality is not changing the atom type of the particles in the topoII section, both simulations should give the exact same results. If this is the case when running **bash.run.sh** at the end the programm will print success.

**Note 2:** By default the lammps script with the topo2 (*run.lam.test.with*) sets the simulation with the keyword static. If you want to test a different keyword uncomment the lines related to fix_topo2 at the end of the file accordingly.
