#!/bin/bash

#Directories
cwd=$(pwd)

#Executable
LAMMPS=~/lmp_serial_29Oct2020_fixtopo2


#Set variables
topofreq=100  #frequency with which topo2 is intorduced
initial=0     #initial time of the simulation
last=10000    #End of simulation
dumpfreq=100  #Dump frequency
topotype=4    #atom type for the atoms in the topoII section
ltopo=50      #length of the topoII
check=0       #flag


#Run system without fix topo2:
  echo "Running system without TopoII"
  cd ${cwd}
  if [ -d "DUMPS_without_topo2" ]; then rm -Rf DUMPS_without_topo2; fi
  
  ${LAMMPS} -in run.lam.test.without
  mv energy_total.txt DUMPS/
  mv radius_of_gyration.txt DUMPS/
  mv log.lammps DUMPS/log.lammps
  mv DUMPS DUMPS_without_topo2


#Run system with fix topo2:
  echo "Running system with TopoII"
  cd ${cwd} 
  if [ -d "DUMPS_with_topo2" ]; then rm -Rf DUMPS_with_topo2; fi
  
  ${LAMMPS} -in run.lam.test.with
  mv energy_total.txt DUMPS/
  mv radius_of_gyration.txt DUMPS/
  mv log.cite DUMPS/
  mv log.lammps DUMPS/log.lammps1
  mv DUMPS DUMPS_with_topo2


#Compare results of the two simulations
for t in $( seq ${initial} ${dumpfreq} ${last} )
do
  cd ${cwd} 
  sim1="DUMPS_without_topo2/Run.R.${t}"
  sim2="DUMPS_with_topo2/Run.R.${t}"

  #If both files are equal set check=1
  diff ${sim1} ${sim2}
  if [ $? -eq 0 ]; then
      check=$(( check+1 ))
  fi
done

##The number of files analysed
nf=$(( ((last-initial)/dumpfreq) + 1 ))

if [ ${check} -eq ${nf} ]; then
  echo "#####################"
  echo "   TEST 5: SUCCESS   "
  echo "#####################"
fi

if [ ${check} -ne ${nf} ]; then
  echo "##################"
  echo "   TEST 5: FAIL   "
  echo "##################"
fi
