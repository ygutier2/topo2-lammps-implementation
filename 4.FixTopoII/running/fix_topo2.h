/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Yair Augusto GutiÃ©rrez Fosado
                         Davide Michieletto
------------------------------------------------------------------------- */

#ifdef FIX_CLASS
FixStyle(topoII,FixTopo2)

#else

#ifndef LMP_FIX_TOPOII_H
#define LMP_FIX_TOPOII_H

#include "fix.h"
#include "pair.h"

namespace LAMMPS_NS {

class FixTopo2 : public Fix {
 public:
  FixTopo2(class LAMMPS *, int, char **);
  ~FixTopo2();
  int setmask();
  void init();
  void post_integrate();
  int *select;
  int *beads;
  void write_restart(FILE *);
  void restart(char *);
  
  int randomid();
  int maxdensid(int);
  int maxcurvid(int);
  double pmove();
  void changetype();
  void placetopo(int);

 private:
  int *alist;
  int flagtopo;
  int nevery;
  int seed;
  double prob;
  int ntopo;
  int ltopo;
  int topotype;
  int tsteptopo;
  int intertype;
 
  class NeighList *list;
  class RanPark *random_equal;
  class RanPark *random_unequal;
  class RanMars *random;
};

}

#endif
#endif
