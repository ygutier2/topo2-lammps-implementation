# fix topoII

Inside each of the folders in this directory you will find the files **fix_topo2.cpp** and **fix_topo2.h**. These files contain the implementation of the new fix. In order to add the implementation to the LAMMPS executable you need to simply:

```
1.- Download the most recent version of LAMMPS (currently this has been tested using the **29-Oct-2020** version).
2.- Unzip the tar file containing the LAMMPS distribution.
3.- Copy and paste the files "fix_topo2.cpp" and  "fix_topo2.h" into the folder "src/" (this folder is part of the standard directories of the lammps distribution).
4.- Make sure that the **MOLECULE** package is installed.
5.- Compile the LAMMPS executable as usual (make mpi or make serial). We renamed the mpi and serial executable as: "lmp_mpi_29Oct2020_fixtopo2" and "lmp_serial_29Oct2020_fixtopo2".
6.- Move the executables to your home directory (~).
```

Please be aware that the *.cpp* and *.h* files inside the folder **testing** are slightly different than the ones inside **running**. This is because the files in the former case allow to print the **position of the topo2** every time the **fix topo II** is called, and in this way we can test if the fix is doing what we expect. In the latter case (running) this information is not printed.

### Syntax

> ```
> fix ID group-ID topoII keyword values ...
> ```

* ID, group-ID are documented in [fix](https://docs.lammps.org/fix.html) command
* topoII = style name of this fix command
* Only one keyword can be used at a time
* keyword = *static* or *randjump* or *jump2maxdens* or *jump2maxcurv*

> *static* values: n, l, topotype, seed, t
>
> * n = the number of topoII in the system. Currently it only accepts 1.
> *  l  = the length in beads of each topoII in the system.
> *  topotype = the topoII particle type.
> *  seed = random seed to generate the random position of the topoII
> *  t' = timestep at which the topoII is simulated into the system.
>
> *randjump*,  *jump2maxdens* and *jump2maxcurv* have the same values: n, l, topotype, seed, freq, prob, intertype.
>
> * n = the number of topoII in the system. Currently it only accepts 1.
> * l  = the length in beads of each topoII in the system.
> * topotype = the topoII particle type.
> * seed = random seed to generate the random position of the topoII (it only has a meaning for *randjump*, in the other cases is ignored).
> * freq = how often the fix is called.
> * prob = probability that the topoII changes position each time the fix is called.
> * intertype: intermediate type assigned to atoms from a section where topoII is jumping from.

### Examples

> fix 0 all topoII static       1 50 4 336644 1000
>
> fix 0 all topoII randjump     1 50 4 336644 1000 0.1 5
>
> fix 0 all topoII jump2maxdens 1 50 4 336644 1000 0.1 5
>
> fix 0 all topoII jump2maxcurv 1 50 4 336644 1000 0.1 5





### Description

This fix changes the atom type of selected beads according to an specific criteria:

* **static**: at timestep *t'*  a bead in the polymer is selected randomly. Then this bead and the (*l-1*) beads "behind it" change type to *topotype*. After this, there is no change in the atom type during the course of the simulation.
* **randjump**: The fix is called  every *freq* time-steps. Every time the fix is called there is a probabiliti (*prob*, between 0 and 1) that certain beads change type. When this happens, three type of beads are defined in the simulation: (i) **intertype** (type 5 in the example), for beads in the region that the topoII is moving from; (ii) **topotype** (type 4 in the example) for beads in the region where the topoII is moving to; (iii) **type 1**, for the rest of the beads representing regions of the polymer free of topoII. The beads that change type to **topotype** are selected in the same way as in the static case.
* **jump2maxdens**: The fix is called  every *freq* time-steps. Every time the fix is called there is a probabiliti (*prob*, between 0 and 1) that certain beads change type. When this happens, three type of beads are defined in the simulation: (i) **intertype** (type 5 in the example), for beads in the region that the topoII is moving from; (ii) **topotype** (type 4 in the example) for beads in the region where the topoII is moving to; (iii) **type 1**, for the rest of the beads representing regions of the polymer free of topoII. The beads that change type to **topotype** are selected in the following way: the bead with maximum number of neighbors, i.e., the one with maximum density, is chosen. Then this bead and the (*l-1*) beads "behind it" change type.
* **jump2maxcurv**: The fix is called  every *freq* time-steps. Every time the fix is called there is a probabiliti (*prob*, between 0 and 1) that certain beads change type. When this happens, three type of beads are defined in the simulation: (i) **intertype** (type 5 in the example), for beads in the region that the topoII is moving from; (ii) **topotype** (type 4 in the example) for beads in the region where the topoII is moving to; (iii) **type 1**, for the rest of the beads representing regions of the polymer free of topoII. The beads that change type to **topotype** are selected in the following way: the bead with maximum curvature is chosen. Then this bead and the (*l-1*) beads "behind it" change type.



# Testing

We have tested that the *serial* and *mpi* executables. Inside **testing** you will find eight folders corresponding to eight different tests we carried out to check that everything is working as expected. Inside each folder there is a README.md file with a description of the test. Each test is run by typing *./bash.run.sh* in a terminal. After the test has finished you should see a **success :)** legend in the terminal, indicating that the test has been passed.

