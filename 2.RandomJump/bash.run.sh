#!/bin/bash

ncpu=64

mf=`pwd`
ff=$mf/MASTERFILES

cd ${ff}
c++ Topo2_Random_Jump.c++ -o Topo2_Random_Jump
cd ${mf}



for i in $( seq 1 ${ncpu} )
do 
  mkdir REP${i}
  cd    REP${i}

  cp ${ff}/run.lam .
  cp -r ${ff}/Init .
  cp ${ff}/Topo2_Random_Jump .
  
  r1=$(od -An -N2 -i /dev/urandom)
  r2=$(od -An -N2 -i /dev/urandom)
  r3=$(od -An -N2 -i /dev/urandom)
  r4=$(od -An -N2 -i /dev/urandom)
  r5=$(od -An -N2 -i /dev/urandom)
  r6=$(od -An -N2 -i /dev/urandom)
  echo "variable seed       equal ${r1}" >  parameters.dat
  echo "variable seed00     equal ${r2}" >> parameters.dat
  echo "variable seedin     equal ${r3}" >> parameters.dat
  echo "variable seedtopo   equal ${r4}" >> parameters.dat
  echo "variable seedthermo equal ${r5}" >> parameters.dat
  echo "variable rate       equal 0.1" >> parameters.dat 

  #nohup ~/lmp_mpi_29Oct2020 -in run.lam < /dev/null > out &
  ~/lmp_mpi_29Oct2020 -in run.lam
  
  cd ../

  vi=$(( ${i}%${ncpu} ))

  if(( ${vi} == 0 )); then
  wait 
  fi
done

wait
