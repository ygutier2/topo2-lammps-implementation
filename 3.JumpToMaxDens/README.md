# TopoII jump to maximum density region

The bash scripts provided in this folder contain the instructions to run 64 independent replica of the same system. For each replica, the topoII position is updated during the course of the simulation and every time this happens the topoII is moved to a the position with maximum density of beads along the polymer contour length. The **bash.run.sh** script contains the instructions to execute the simulation. This script makes use of the files located inside the folder **MASTERFILES** which contains:

* The Initial configuration data file (*Eq.5_1.N1000.data*), located inside the folder *Init*.
* The LAMMPS script (*run.lam*) tor run the simulations from the initial data file.
* The external program (*Topo2_maxdens_jump.c++*).  

Also, it is assumed that the LAMMPS executable is called *lmp_mpi_29Oct2020* and is located in the user's home directory with path  (*~/lmp_mpi_29Oct2020*). 

### The bash.run.sh file

In this file:

> * It is assumed that the simulations are run in a cluster with 64 processors available. If this is not the case, users can change the value of the variable *ncpu=64* according to their needs. This variable also sets the number of independent simulations to run. The results from these simulations are stored into the folders REP1, REP2, ..., REP64. Each of the 64 simulations:
>   * Runs in serial.
>   * Starts from the same initial configuration, *Eq.5_1.N1000.data* and runs for an equilibration time before introducing the topoII region.
> * There are instructions to produce the file *parameters.dat* which contains different **random seeds** that are used to produce independent simulations. Also the variable **rate** is defined, this is related to the probability that the topoII jumps to a different position during the course of the simulation (every time the external programm is called, see below for details).

### The run.lam script

> * The initial configuration data file contains a bead-and-spring polymer made by 1000 beads of type 1. In the LAMMPS script a Lennard Jones repulsive potential is set between these type of particles so they can't overlap or cross through each other. Consecutive beads are connected via FENE bonds.
> * The simulation is run for *run1=1000000 timesteps*. Then a restart file *dataRestart.Run.R1.0* is created.
> * At this point, the external program (**Topo2_maxdens_jump.c++**) is called for the first time within LAMMPS. It introduces the topoII randomly along the polymer. This is done by changing the bead type of 50 consecutive beads from type 1 to type 4. After this, the program is called every 1000 timesteps. Every time that the program is called, there is a probability that the position of the topoII is updated to the position of maximum bead density. When this happens, the external program defines three type of beads: (i) **type 5**, for beads in the region that the topoII is moving from; (ii) **type 4** for beads in the region where the topoII is moving to; (iii) **type 1**, for the rest of the beads representing regions of the polymer free of topoII. 
> * The arguments required by **Topo2_maxdens_jump.c++** are: timestep, input-file, output-file, rate, random-seed. The **timestep** represents the current timestep in the simulation at which the external programm is called. The **input-file** (*dataRestart.Run.R1.timestep*) is the restart file created at the current timestep. The **output-file** (*newdataRestart.Run.R1.timestep*) is the system obtained after the external program is applied to the configuration in the input-file. The **rate** is set to a fixed value of 0.1, so 10% of the times that the external program is called the position of the topoII will change. Finally, it is important to note that **the value of the variable Nbeads in the file Topo2_maxdens_jump.c++ must be set by hand to the total number of beads in the system, in this case, it has already been set to 1000**.
>
> * The output from the simulation is stored in the folder DUMPS.

It is important to mention also that:

>* In section *Parameters*, lines 32, 37, 242 and 243  in the lammps script, the variables *run1, run2, dtime2 and dtime2d* have been commented using a leading # character. The values in the commented lines correspond to the values set in the original simulations. The new values (without the # character) are used only to run shorter simulations for testing purposes.
>* The persistence length of the polymer is controlled by the magnitude of the second parameter introduced in the section *angle_coeff* (lines 84 and 225 in the script). This value should be given in units of sigma. Therefore, **angle_coeff 1 20** represents a polymer with lp=20 Ï=50 nm.
