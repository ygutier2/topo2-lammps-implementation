# Static topoII

The bash scripts provided in this folder contain the instructions to run 64 independent replica of the same system. For each replica, the topoII section is placed randomly along the polymer contour length. The **bash.run.sh** should be executed only when the simulation runs for the first time. If you want to continue a simulation you should then execute the file **bash.run.restart.sh**. This file will continue the simulation from the last restart file previously created. These scripts make use of the files located inside the folder **MASTERFILES** which contains:

* The LAMMPS scripts tor run the simulations from the initial data file, *run.lam*, and to restart a simulation, *run.lam.restart*.
* The external program (*Topo2_random_region.c++*) that places the topoII randomly along the polymer by changing the bead type of 50 consecutive beads, from type 1 to type 4.
* The Initial configuration data file (*Eq.5_1.N1000.data*), located inside the folder *Init*.

Also, it is assumed that the LAMMPS executable is called *lmp_mpi_29Oct2020* and is located in the user's home directory with path  (*~/lmp_mpi_29Oct2020*). 

## Run from initial configuration data file

### The bash.run.sh file

In this file:

> * It is assumed that the simulations are run in a cluster with 64 processors available. If this is not the case, users can change the value of the variable *ncpu=64* according to their needs. This variable also sets the number of independent simulations to run. The results from these simulations are stored into the folders REP1, REP2, ..., REP64. Each of the 64 simulations:
>   * Runs in serial.
>   * Starts from the same initial configuration, *Eq.5_1.N1000.data* and runs for an equilibration time before introducing the topoII region.
> * There are instructions to produce the file *parameters.dat* which contains different random seeds that are used to produce independent simulations.

### The run.lam script

> * The initial configuration data file contains a bead-and-spring polymer made by 1000 beads of type 1. In the LAMMPS script a Lennard Jones repulsive potential is set between these type of particles so they can't overlap or cross through each other. Consecutive beads are connected via FENE bonds.
> * The simulation is run for *run1=1000000 timesteps*. Then a restart file *dataRestart.Run.R1.0* is created.
> * After this, we use the program **Topo2_random_region.c++** to place the topo II  randomly along the DNA contour length. The arguments required by this programs are: timestep, input-file, output-file, random-seed. The input-file (*dataRestart.Run.R1.0*) is the restart file that comes from the previous equilibration. The output-file (*newdataRestart.Run.R1.0*) contains the position of the topo II represented by 50 beads of type 4 that interact through a soft potential, allowing strand crossing. The random-seed can be used to choose the random position of the initial bead in the topoII section. For the static case the timestep is always 0, indicating that after the initial equilibration the topo II is placed and will not be moved. Finally, it is important to note that **the value of the variable Nbeads in the file Topo2_random_region.c++ must be set by hand to the total number of beads in the system, in this case, it has already been set to 1000**.
> * The output from the simulation is stored in the folder DUMPS.

It is important to mention also that:

>* In section *Parameters*, from line 22 in the lammps script, some variables have been commented using a leading # character. The values in the commented lines correspond to the values set in the original simulations. The new values (without the # character) are used only to run shorter simulations for testing purposes.
>* The persistence length of the polymer is controlled by the magnitude of the second parameter introduced in the section *angle_coeff* (lines 71 and 176 in the script). This value should be given in units of sigma. Therefore, **angle_coeff 1 20** represents a polymer with lp=20 σ=50 nm.


## Continue a simulation from the last restart file created

### The bash.run.restart.sh file

It contains the instructions to check the timestep in the simulation at which the last restart file was created. Note that the restart files can be found inside the folder *REP1/Data/*, for example, for the first replica. The value of this timestep is then automatically assigned to the variable *readingtime* inside the *run.lam.restart* script. From there this script is pretty much the same as the previously described *run.lam*.

