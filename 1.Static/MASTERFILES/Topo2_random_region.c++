#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include <unistd.h>
#include<functional>
#include<numeric>
#include<cstdlib>
#include <algorithm>
using namespace std;

int Ntot,ntype;
double Lmaxx,Lminx,Lmaxy,Lminy,Lmaxz,Lminz;
double Lx,Ly,Lz;
string dummy;
int nangles,nangletypes,nbonds,nbondtypes;
//
int id,mol,type;
double x,y,z;
int ix,iy,iz;
double vx,vy,vz;
//
const int N=1;
const int Nbeads=1000;
double Polymer[N][Nbeads][6]; //x,y,z,mol,type,id
double PolymerWrap[N][Nbeads][3]; //x,y,z
int PolymerImg[N][Nbeads][3]; //ix,iy,iz
double Velocity[N][Nbeads][3];
int Angles[Nbeads][4]; //bead1,bead2,bead3,atype
int Bonds[Nbeads+1][3]; //bead1,bead2,btype


double distance(double v1[6], double v2[6]);
int main(int argc, char* argv[]){
cout << " ############################# EXTERNAL LOOP TOPOII ################### " <<endl;

//ARGV[1]=timestep
//ARGV[2]=infile-no time
//ARGV[3]=outfile-no time
//ARGV[4]=Random seed
int time=atoi(argv[1]);
int seedr=atoi(argv[4]);



//READ FILE
ifstream indata;
stringstream readFile;
readFile.str("");
readFile.clear();
readFile << argv[2] << argv[1];
indata.open(readFile.str().c_str());
cout << readFile.str().c_str()<<endl;
if(!indata.good()){cout << "Could not find file"<<endl; cin.get();cin.get();cin.get();}

//read lines
for(int i=0;i<12;i++){
if(i==2) {
indata >> Ntot;
}
if(i==3){
indata >> ntype;
}
if(i==4){
indata >> nbonds;
}
if(i==5){
indata >> nbondtypes;
}
if(i==6){
indata >> nangles;
}
if(i==7){
indata >> nangletypes;
}
if(i==9) {
indata >> Lminx >> Lmaxx;
Lx = Lmaxx-Lminx;
}
if(i==10) {
indata >> Lminy >> Lmaxy;
Ly = Lmaxy-Lminy;
}
if(i==11) {
indata >> Lminz >> Lmaxz;
Lz = Lmaxz-Lminz;
}
else{
getline(indata,dummy);
}
}

while(1==1){
getline(indata,dummy);
string str=dummy;
string str2 ("Atoms");
if (str.find(str2) != string::npos)break;
}

//READ ATOMS
for(int n=0; n<Ntot; n++){
	indata >> id>> mol>>type>> x>>y>>z>>ix>>iy>>iz;
	PolymerWrap[0][id-1][0]=x;
	PolymerWrap[0][id-1][1]=y;
	PolymerWrap[0][id-1][2]=z;
	PolymerImg[0][id-1][0]=ix;
	PolymerImg[0][id-1][1]=iy;
	PolymerImg[0][id-1][2]=iz;
	//
	Polymer[0][id-1][0]=x+Lx*ix;
	Polymer[0][id-1][1]=y+Ly*iy;
	Polymer[0][id-1][2]=z+Lz*iz;
	Polymer[0][id-1][3]=mol;
	Polymer[0][id-1][4]=type;
	Polymer[0][id-1][5]=id;
}

//READ VELOCITIES
indata >> dummy;
for(int n=0; n<Ntot; n++){
	indata >> id >> vx>>vy>>vz;
	Velocity[0][id-1][0]=vx;
	Velocity[0][id-1][1]=vy;
	Velocity[0][id-1][2]=vz;
}

//READ BONDS
int nbonds0=Nbeads;
int bid,btype,b1,b2;
bid=0;btype=0;b1=0;b2=0;
for(int n=0;n<nbonds;n++)for(int p=0;p<3;p++)Bonds[n][p]=0;
indata >> dummy;
for(int nb=0; nb<nbonds; nb++){
	indata >> bid >> btype>>b1>>b2;
	Bonds[bid-1][0]=b1;
	Bonds[bid-1][1]=b2;
	Bonds[bid-1][2]=btype;
}

//READ ANGLES
int aid,atype,a1,a2,a3;
aid=0;atype=0;a1=0;a2=0;a3=0;
for(int na=0;na<nangles;na++)for(int pa=0;pa<4;pa++)Angles[na][pa]=0;
indata >> dummy;
for(int na=0; na<nangles; na++){
	indata >> aid >> atype>>a1>>a2>>a3;
	Angles[na][0]=a1;
	Angles[na][1]=a2;
	Angles[na][2]=a3;
	Angles[na][3]=atype;
}


//////////////////
///// TOPO 2 /////
//////////////////
//Created the topoII region only at the beggining of the simulation (timestep=0), after calling this programm from inside LAMMPS.
//This is done by changing the type of particles in the topoII region to type 4.
if(time==0){
int loc;
loc = rand() % Nbeads;

int wtopo2=50;
for (int i=0;i<wtopo2;i++){
int j=(loc-wtopo2+i);
if(j<0)j+=Nbeads;
Polymer[0][j][4]=4;
}
}

/////////////////////////////////////////////////////////////////////////
///////////////////	WRITE NEW FILE  /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

//OUTFILE
stringstream writeFile;
writeFile <<argv[3]<<argv[1];
ofstream write(writeFile.str().c_str());
cout << "writing on .... " <<writeFile.str().c_str()<<endl;
write.precision(19);
write << "LAMMPS data file via write_data, version 15 May 2015, timestep = "<< argv[1]<< endl;
write <<endl;
write << Ntot  << " atoms "<<endl;
write << ntype  << " atom types "<<endl;
write << nbonds << " bonds "<<endl;
write << nbondtypes << " bond types "<<endl;
write << nangles << " angles "<<endl;
write << nangletypes << " angle types "<<endl;
write << "\n";
write << Lminx << " " << Lmaxx << " xlo xhi"<<endl;
write << Lminy << " " << Lmaxy << " ylo yhi"<<endl;
write << Lminz << " " << Lmaxz << " zlo zhi"<<endl;
write << "\nMasses \n"<<endl;
for(int j=0; j<ntype;j++) write << j+1 << " " << 1<< endl;

write << "\nAtoms \n"<<endl;
for(int n=0; n<Ntot;n++) write << Polymer[0][n][5] << " " << Polymer[0][n][3] << " " <<  Polymer[0][n][4] << " " << PolymerWrap[0][n][0] << " " <<PolymerWrap[0][n][1] << " " << PolymerWrap[0][n][2] << " " << PolymerImg[0][n][0] << " " << PolymerImg[0][n][1] << " " << PolymerImg[0][n][2] << endl;


write << "\n\n\nVelocities\n"<<endl;
for(int n=0; n<Ntot;n++) write << n+1 << " " << Velocity[0][n][0] << " "<< Velocity[0][n][1] << " "<<  Velocity[0][n][2] << endl;


write << "\nBonds \n"<<endl;
for(int n=0; n<nbonds;n++) {
write << n+1 << " " << Bonds[n][2] << " " << Bonds[n][0] << " " << Bonds[n][1] << endl;
}

write << "\nAngles \n"<<endl;
for(int na=0; na<nangles;na++){
write << na+1 << " " << Angles[na][3] << " " << Angles[na][0] << " " << Angles[na][1] << " " << Angles[na][2]<< endl;
}
cout << " ###################################### FIN ############################## " <<endl;

return 0;

}

double distance(double v1[6], double v2[6]){
double d=0;

d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);

return sqrt(d);
}
