#!/bin/bash

ncpu=64

mf=`pwd`
ff=$mf/MASTERFILES

cd ${ff}
c++ Topo2_random_region.c++ -o Topo2_random_region
cd ${mf}



for i in $( seq 1 ${ncpu} )
do 
  mkdir -p REP${i}
  cd    REP${i}

  cp ${ff}/run.lam.restart .
  cp ${ff}/Topo2_random_region .
  
  ##Check the last restart file created in the previous run (example: Restart.topo2.100000000)
  cd Data/
  LastRFile=$(ls -tr|tail -1)
  cd ../
  #Obtain the timestep part of the name of the previous file (example: 100000000)
  rtstep=$( echo "${LastRFile##*.}" )
  echo ${rtstep}
  #The DT parameter inside our lammps script 
  Dt=1000
  #Therefore the loop variable should start at:
  lstart=$( echo $((rtstep / Dt)) )
  
  #Replace the line related to reading the restart file
  sed -i "s/variable readingtime equal .*/variable readingtime equal ${rtstep}/g" run.lam.restart
  
  r1=$(od -An -N2 -i /dev/urandom)
  r2=$(od -An -N2 -i /dev/urandom)
  r3=$(od -An -N2 -i /dev/urandom)
  r4=$(od -An -N2 -i /dev/urandom)
  r5=$(od -An -N2 -i /dev/urandom)
  echo "variable seed       equal ${r1}" >  parameters.dat
  echo "variable seed00     equal ${r2}" >> parameters.dat
  echo "variable seedin     equal ${r3}" >> parameters.dat
  echo "variable seedtopo   equal ${r4}" >> parameters.dat
  echo "variable seedthermo equal ${r5}" >> parameters.dat

  #nohup ~/lmp_mpi_29Oct2020 -in run.lam.restart < /dev/null > out &
  ~/lmp_mpi_29Oct2020 -in run.lam.restart
  
  cd ../

  vi=$(( ${i}%${ncpu} ))

  if(( ${vi} == 0 )); then
  wait 
  fi
done

wait